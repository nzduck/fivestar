# STARS Design Pattern Decisions


## Implementations

There are a few main concerns when it comes to secure document signing on the Ethereum blockchain:

- **Pagination Save Cost.**

To retreive data such as list comments and companies can be prohibitively expensive on the Ethereum blockchain at the current state. 
> STARS uses pagination design patten to get items by specific page number and page size, which could save the cost.

- **Emergency Stop and Upgradability**

In case of catastrophic failure and bugs on the contract, the contract should be able to be stopped and upgraded.
> STARS implements emergency stop pattern that allows the contract to be paused and upgraded in events of failure.

## Future Improvements
- **Only allow certain user to view comments.**

This feature could be desirable as some comments may contain valuable information one does not wish to share with others. 

- **Only allow certain user to maintain companies.**

This could be implemented by role management, give the owner possbility to maintaine the companies and comments belong to the owner.