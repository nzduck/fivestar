var Stars = artifacts.require("Stars");

contract("Stars Contract test", async accounts => {
  const account_0 = accounts[0];
  const ipfsLogo = "0x424oj2gu5k2hb3442b3";
  const latitude = "-368.47500";
  const longitude = "174.757614";
  const total = 100;

  it("Allows user to create a company", async () => {
    let instance = await Stars.deployed();
    var event = instance.CompanyCreateLog();
    await event.watch((err, res) => {
      companyId = res.args.id.toString(10);
      totalCompanies = res.args.totalCompanies.toString(10);
    });
    var n = 0;
    while (n < total) {
      await instance.createCompany("TVNZ" + n, ipfsLogo, latitude, longitude, {
        from: account_0
      });
      n++;
    }
    const firstPage = await instance.getCompanyIdsPagination.call(0, 20);    
    assert.equal(firstPage[0], total, "The total number of companies does not match the init number");
  });

  it("Allows user to get companies by specific page number", async () => {
    let instance = await Stars.deployed();
    var event = instance.CompanyCreateLog();
    var startIndex = 0;
    var limit = 20;
    var nextIndex = 20;
    const secondPage = await instance.getCompanyIdsPagination.call(nextIndex, limit);    
    assert.equal(secondPage[0],total,"The total number of companies does not match the init number");
    assert.equal(secondPage[1].length.toString(10),limit,"The result of first page does not match the page limit");
    assert.equal(secondPage[2].toString(10),40,"The next start index is not expected");
  });

  it("Allows user to get a company by Id", async () => {
    let instance = await Stars.deployed();
    const result = await instance.getCompanyById.call(50);
    assert.equal(result[0], 50, "The id of the company does not match");
    assert.equal(result[1], "TVNZ50", "The name of the company does not match");
    assert.equal(result[2], ipfsLogo, "The logo of the company does not match");
    assert.equal(result[3], latitude, "The latitude of the company does not match");
    assert.equal(result[4], longitude, "The longitude of the company does not match");
    assert.equal(result[5], 0, "The totalStars of the company does not match");
    assert.equal(result[6], 0, "The totalCount of the company does not match");
    assert.equal(result[7], account_0, "The author of the company does not match");
  });

  it("Allows user to comment a company with star scores", async () => {
    let instance = await Stars.deployed();
    const companyId = 5;
    const star = 5;
    const comment = "This is a good company";
    await instance.addComment(companyId, star, comment, {from: account_0});
    const result = await instance.getCommentByCompanyIdAndCommentId.call(companyId, 0);
    assert.equal(result[0].toString(10), companyId, "The company Id does not match");
    assert.equal(result[1].toString(10), 1, "The total number of comments does not match");
    assert.equal(result[2].toString(10), star, "The star of comments does not match");
    assert.equal(result[3], comment, "The comment does not match");
    assert.equal(result[4], account_0, "The author of the company does not match");
  });

  it("Allows user to view 10 companies on a page", async () => {
    let instance = await Stars.deployed();
    var startIndex = 20;
    var limit = 10;
    const page = await instance.getCompanyIdsPagination.call(startIndex, limit);
    var ids = page[1];    
    var data = []
    for (let id of ids) {
      var companyId = id.toString(10);      
      const result = await instance.getCompanyById.call(companyId);      
      var company = {
        id: result[0].toString(10),
        name: result[1],
        ipfsLogo: result[2],
        latitude: result[3],
        longitude: result[4],
        totalStars: result[5].toString(10),
        totalCount: result[6].toString(10),
        author: result[7]
      }
      data.push(company)      
    }    
    assert.equal(data.length, limit, "The number of companies does not match");
  });


  it("Allows user to view all the comments of a specific company on the page", async () => {
    let instance = await Stars.deployed();
    let companyId = 20;
    const star = 5;    
    const comment = "This is a good company";
    for(let i=0; i<20; i++) {
      await instance.addComment(companyId, star, comment + i, {
        from: account_0
      });
    }
    const result = await instance.getCompanyById.call(companyId);  
    const totalComments  = result[6].toString(10);        
    var comments = [];
    for(let i=0; i<totalComments; i ++) {
      const result = await instance.getCommentByCompanyIdAndCommentId.call(companyId, 0);
      comments.push({        
        star: result[2].toString(10),
        text: result[3],
        author: result[4]
      })      
    }       
    assert.equal(comments.length, 20, "The number of comments does not match");
  });  
});