pragma solidity 0.4.24;

import "./CommentsLib.sol";

library CompaniesLib {
    
    using CommentsLib for CommentsLib.Commment;

    // Create a struct named Company with properties id, name, logo, star, 
    struct Company {
        uint id; //Company id, index Id
        string name; //Company name
        string ipfsLogo; //Company logo store in the IPFS
        string latitude; //Company address latitude
        string longitude; //Company address longitude
        uint totalStars; //Sum of stars for everyone's rating
        uint totalCount; //Sum of rating times    
        address author;  //The author create the Company Profile
        //A mpping to save the comments belong to this specific company
        mapping(uint => CommentsLib.Commment) comments;
    }

    // Create a company for users to rating
    function addCompany(uint _id, string _name, string _ipfsLogo, string _latitude, string _longitude) 
        internal
        view
        returns(Company)
    {        
        return Company({id:_id, name:_name, ipfsLogo:_ipfsLogo, latitude:_latitude,longitude:_longitude,totalStars:0, totalCount:0, author:msg.sender});
    }

}