pragma solidity 0.4.24;

library CommentsLib {
    
    struct Commment {
        uint id;
        uint star; //The author of star 
        string comment;  //The comment content
        address author;  //The author of the comment       
    }

    //Add a comment for the company
    function addComment(uint _id, uint _star, string _comment) 
        internal
        view
        returns(Commment)
    {        
        return Commment({id:_id, star:_star, comment:_comment, author:msg.sender});
    }   

}