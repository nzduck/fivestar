pragma solidity ^0.4.23;

import "./lib/Destructible.sol";
import "./lib/Pausable.sol";
import "./core/CompaniesLib.sol";

contract Stars is Destructible, Pausable {

    //Imppot comanpaies and comments lib
    using CompaniesLib for CompaniesLib.Company;
    using CommentsLib for CommentsLib.Commment;    
    //A counter to count total companies
    uint public totalCompanies;
    //Using mapping to store companies
    mapping(uint => CompaniesLib.Company) companies;
    //Using array to store all the company IDs for the pagination purpose
    uint[] companyIds;

    event CompanyCreateLog(uint id, uint totalCompanies);
      
    constructor() public {    
        //Init totalCOmpanies is 0
        totalCompanies = 0;
    }
   
    //Create a new company for the using rating
    function createCompany(string _name, string _ipfsLogo, string _latitude, string _longitude) 
        public
        whenNotPaused
        returns (uint)
    {
        uint _id = totalCompanies;
        companies[_id] = CompaniesLib.addCompany(_id, _name, _ipfsLogo, _latitude, _longitude);
        companyIds.push(_id);
        totalCompanies += 1;
        emit CompanyCreateLog(_id, totalCompanies);  
        return totalCompanies;
    }

    //Get company Ids pagination, with curson and page size
    function getCompanyIdsPagination(uint cursor, uint limit)
        public
        view
        whenNotPaused
        returns (uint total, uint[] pagedCompanyIds, uint newCursor)
    {
        uint length = limit;
        total = companyIds.length;
        if (length > total - cursor) {
            length = total - cursor;
        }

        pagedCompanyIds = new uint[](length);
        for (uint i = 0; i < length; i++) {
            pagedCompanyIds[i] = companyIds[cursor + i];
        }
        
        return (total, pagedCompanyIds, cursor + length);
    }
    
    //Get company by specific Id
    function getCompanyById(uint _id) 
        public 
        view
        whenNotPaused
        returns (uint id, string name, string ipfsLogo, string latitude, string longitude, uint totalStars, uint totalCount, address author) 
    {
        id = companies[_id].id; 
        name = companies[_id].name;
        ipfsLogo = companies[_id].ipfsLogo;
        latitude = companies[_id].latitude;
        longitude = companies[_id].longitude;
        totalStars = companies[_id].totalStars;
        totalCount = companies[_id].totalCount;
        author = companies[_id].author;                       
        return (id, name, ipfsLogo, latitude, longitude, totalStars, totalCount, author);
    }

    //Add comment for a company by specific
    function addComment(uint _id, uint _star, string _comment) 
        public
        whenNotPaused
        returns (uint)
    {        
        uint _cid = companies[_id].totalCount;
        companies[_id].comments[_cid] = CommentsLib.addComment(_id, _star, _comment);
        companies[_id].totalCount += 1;    
        companies[_id].totalStars += _star;   
        return companies[_id].totalCount;
    }

    //get comment by companyId and comment Id
    function getCommentByCompanyIdAndCommentId(uint _companyId, uint _commentId) 
        public 
        view
        whenNotPaused
        returns (uint companyId, uint totalComments, uint star, string comment, address author) 
    {
        companyId = _companyId;        
        star = companies[companyId].comments[_commentId].star; 
        comment = companies[companyId].comments[_commentId].comment;
        author = companies[companyId].comments[_commentId].author;
        totalComments = companies[companyId].totalCount;
        return (companyId, totalComments, star, comment, author);   
    }
}