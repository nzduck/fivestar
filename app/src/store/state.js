let state = {
  web3: null,
  token: localStorage.getItem('access_token') || null,
  contractInstance: null  
};
export default state;
