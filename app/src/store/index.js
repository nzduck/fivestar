import Vue from "vue";
import Vuex from "vuex";


import { Connect, SimpleSigner } from "uport-connect";
const uport = new Connect("Joe's new app", {
  clientId: "2oiGrkQipA7Qgkrj2iL1DX3TNEbjkctaeEK",
  network: "rinkeby",
  signer: SimpleSigner(
    "9f398b7aca58e7a0d4ff570c7ddb1dfc4c1b4d0b873c464669de0c7ab9bff63d"
  )
});
const web3 = uport.getWeb3();

Vue.use(Vuex);
export const store = new Vuex.Store({
  strict: true,
  state: {
    web3: {},
    token: null    
  },
  getters: {
    loggedIn(state) {
      if(state.token){
        return state.token !== null;
      }
      
    }
  },
  mutations: {
    retrieveToken(state, token) {
      state.token = token;
    },
    destroyToken(state) {
      state.token = null;
    },
    registerWeb3Instance (state, payload) {       
      state.web3 = payload          
    },
  },
  actions: {
    retrieveToken(context) {
      uport
        .requestCredentials({
          requested: ["name", "phone", "country"],
          notifications: true // We want this if we want to recieve credentials
        })
        .then(credentials => {
          //   console.log(credentials.pushToken);
          const token = credentials.pushToken;
          localStorage.setItem("access_token", token);
          context.commit("retrieveToken", token);
        });
      // console.log(credentials.data)
      // console.log('i am login')
    },
    destroyToken(context) {
      // console.log(this.state.token)
      localStorage.removeItem("access_token");
      context.commit("destroyToken");
    },
    async registerWeb3({ commit }, payload) {
      console.log("registerWeb3 Action being executed");
      commit("registerWeb3Instance", payload);
    }
  }
});
