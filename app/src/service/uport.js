import { Connect, SimpleSigner } from "uport-connect";

export const uport = new Connect("David's new app", {
  clientId: "2oiGrkQipA7Qgkrj2iL1DX3TNEbjkctaeEK",
  network: "rinkeby",
  signer: SimpleSigner(
    "9f398b7aca58e7a0d4ff570c7ddb1dfc4c1b4d0b873c464669de0c7ab9bff63d"
  )
});

export const web3 = uport.getWeb3();
