import Vue from 'vue';
import Router from 'vue-router';
import home from '../components/home-page';
import company from '../components/company-page';
import companyPanel from '../components/company-panel';


Vue.use(Router);

const routes = [{
  path: '/',
  redirect: '/home'
}, {
  path: '/home',
  component: home
}, {
  name: 'company',
  path: '/company/:id',
  props: true,
  component: company
},{  
  path: '/company',  
  component: companyPanel
}];

export default new Router({
    linkActiveClass: 'active',
    routes
});