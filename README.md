# STARS - STARS for the Companies & Vendors & Restaurants

![logo](images/home.png)


**Some sample use cases**:

- [Support Uport Login]()

![addCompany](images/uport.png)

- [Register Companies & Vendors & Restaurants in the Dapp]()

![addCompany](images/addCompany.png)

- [Locate the Companies & Vendors & Restaurants on the Google Map]()

![listCompany](images/list.png)

- [Rating the Companies & Vendors & Restaurants with Stars]()

![rating](images/rating.png)

- [Comments added for each Companies & Vendors & Restaurants]()

![rating](images/comment.png)

## Running the STARS Locally
- To compile the contract, run `truffle compile`.
- To deploy the contract to ganache, run `truffle migrate` while Ganache is open.
- To run unit test, run `truffle test` while Ganache is open.
- run `cd app`
- run `npm install`
- run `npm run dev` which should fire up a server on `http://localhost:8080/`
- Log into your Ganache ethereum account on Metamask in your prefered browser, visit `http://localhost:8080/` and play away!
- Files/LOGOS are stored on IPFS.
- Identity management is handled using uPort.

## Running the Tests

Make sure you are on the root folder level and have Ganache installed, then run `truffle test`. The test file itself is stored at `/test/stars.test.js` where you can read up on the detailed explanation of each test.

## Questions and Issues

Please feel free to open issues at this repo if you have any questions!
