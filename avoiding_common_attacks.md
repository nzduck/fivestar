# How STARS Avoid Common Attacks

- **Emergency Stop**

In case of a critical bug, the contract owner can set the contract to be paused and update the front end up to operate on an upgraded and bug-fixed contract. Historically companies and comments can still be retrieved via events.

- **Logic Bugs**
Avoid overly complex rules.Follow Solidity coding standards and best practices. All require statements are checked before implementing any storage change. Transactions that do not meet requirements will be reverted.

- **Simple Design**

The STARS contract was designed with intentional simplicity so the code can be easily interpreted, which makes it easy to identify bugs.

- **Avoid Ether Handling**

STARS does not have any payable methods thus there are no direct monetary gains from attacking the contract. All monetary transactions releated to comments and companies should be executed from other external contracts by listening to the appropriate STARS events.